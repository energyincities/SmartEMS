import pandas as pd
import functions as fct




### Import Data
df_weather = pd.read_excel('output_incl_coverage.xlsx')


df_weather = df_weather.ix[8300:16000,:]
print(len(df_weather))
### Select Data
data = df_weather.ix[:,["temp","humid","wind", "dew_point","cloud_cover_8","visibility",
                            "Elevation","Azimuth","SumCh3Ch4"
                            ]]
xlabel=["Temperature","Relative \n humidity","Wind speed", "Dew point \n temperature","Cloud cover","Visibility",
                            "Elevation","Azimuth","Cooling \n demand",
                            ]
fct.heatmap(data, xlabel=xlabel)