##################################################################
        # Function File to File #
        # MLTest_ScikitLearn #
##################################################################
import pandas as pd
import numpy as np

#### I) Derive Feature Vector
def pred_lag_Features(df, tau=0, tau_p=0,inputs_l=None,inputs_p=None, outputs_p=None):

    # create lagged inputs
    if inputs_l:
        for col in inputs_l:
            df_temp = df[col]
            for i in range(1,tau+1):
                mystr = '_lagged%d' %i
                df = df.join(df_temp.shift(i),rsuffix = mystr)
        # Update inputs_l strings
        inputs_l = [ii + "_lagged%d" % j for ii in inputs_l for j in range(1, tau+1)]
    else:
        inputs_l = []

    # create predicted inputs (simply shifting of historical values)
    if inputs_p:
        for col in inputs_p:
            df_temp = df[col]
            for i in range(1, tau_p+1):
                mystr = '_predicted%d' % i
                df = df.join(df_temp.shift(-i), rsuffix=mystr)
        # Update inputs_p strings
        inputs_p = [i + "_predicted%d" % j for i in inputs_p for j in range(1, tau_p+1)]
    else:
        inputs_l = []


    if outputs_p:
        for col in outputs_p:
            df_temp = df[col]
            for i in range(1, tau_p):
                mystr = '_predicted%d' % i
                df = df.join(df_temp.shift(-i), rsuffix=mystr)
        # Update inputs_p strings
        outputs_p = [i + "_predicted%d" % j for i in outputs_p for j in range(1, tau_p)]
    else:
        outputs_p = []

    df = df.ix[tau:len(df) - tau_p,:]

    return df, inputs_l, inputs_p, outputs_p

#### II) Apply mean normalization
def meanNormalization(X,mean=None,std=None):

    if mean is None:
        mean = X.mean(axis=0)
        std = X.std(axis=0)

    X_norm = (X-mean)/std
    return X_norm,mean,std


def reversedmeanNormalization(X_norm, mean, std):
    data = np.empty([len(X_norm.values[:,1]),len(X_norm.values[1,:])])

    for i in range(len(mean)):
        data[:,i] = X_norm.values[:,i]*std[i]+mean[i]

    X = pd.DataFrame(data, index=X_norm.index, columns=X_norm.columns)
    return X




def splt_data(X, y, testfrac, mode=[]):
    # This function either splits samples chronologically (last block test set)
    # or randomly
    if mode=='chronological':
            split=round(len(X[:,1])*(1-testfrac))
            X_train=X[0:split,:]
            y_train=y[0:split]
            X_test=X[split+1:,:]
            y_test=y[split+1:]
    elif mode=='shoulderweek':
            split=testfrac
            split_end=split+24*14
            binary = np.ones(len(X), dtype=bool)
            binary[split:split_end]=0
            X_train=X[binary,:]
            y_train=y[binary]
            X_test=X[binary!=1,:]
            y_test=y[binary!=1]
    else:
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=testfrac)

    return [X_train, X_test, y_train, y_test]




def predplot(y_test=None, y_train=None, y_hat=None, y_hat_train=None, fig_name=None):
    import matplotlib.pyplot as plt
    import datetime
    # Plot 10% deviation lines
    t = np.arange(0., 2850, 100)

    if fig_name:
        plt.figure(fig_name)
        file_name = fig_name
    else:
        plt.figure()
        file_name = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    if len(plt.gca().lines)<3: # check if error lines have been plotted already
        plt.plot(t, 1.1 * t, 'r-', t, .9 * t, 'r-', t, t, 'r--', linewidth=.5)
        plt.plot(t, 1.3 * t, 'r-', t, .7 * t, 'r-', t, t, 'r--', linewidth=.5)
        plt.title('Predicted vs Real Cooling Demand')
        # location to place text
        l2 = np.array((2800, 2800))
        trans_angle = plt.gca().transData.transform_angles(np.array((45,)), l2.reshape((1, 2)))[0]
        plt.text(2800, 2800, r'0  % error', {'ha': 'left', 'va': 'bottom'}, rotation=trans_angle)
        l2 = np.array((2850, 2500))
        trans_angle = plt.gca().transData.transform_angles(np.array((45,)), l2.reshape((1, 2)))[0]
        plt.text(2850, 2500, r'10 % error', {'ha': 'left', 'va': 'bottom'}, rotation=trans_angle)
        l2 = np.array((2850, 1900))
        trans_angle = plt.gca().transData.transform_angles(np.array((45,)), l2.reshape((1, 2)))[0]
        plt.text(2850, 1900, r'30 % error', {'ha': 'left', 'va': 'bottom'}, rotation=trans_angle)
        plt.ylabel('Pred. cooling demand [t]')
        plt.xlabel('Real cooling demand [t]')
        plt.ylim(0, 3400)
        plt.xlim(0, 3400)


    if (np.any(y_test==None)):
        pass
    else:
        plt.plot(y_test,y_hat,'x',alpha=0.8)

    if (np.any(y_train==None)):
        pass
    else:
        plt.plot(y_train,y_hat_train,'x',alpha=0.8)


    plt.savefig("predvsreal_" + file_name + ".pdf")

    return

def predplot_time(y_real=None,y_hat=None,pltrange=None):
    import matplotlib.pyplot as plt
    import datetime
    if pltrange:
        pass
    else:
        pltrange=len(y_real)

    t_arr = np.arange(0., pltrange, 1)
    plt.figure()

    if len(y_real[1,:])>1:
        t_horizon = np.arange(0., len(y_real[1,:]), 1)
        for i in range(pltrange):
            plt.plot(t_horizon, y_real[i,:], 'r-')
            plt.plot(t_horizon, y_hat[i,:], 'b-')
            t_horizon = t_horizon+1
    else:
        plt.plot(t_arr, y_real[:pltrange],'r-')
        plt.plot(t_arr, y_hat[:pltrange], 'b-')
    plt.xlabel('Hour h')
    plt.ylabel('Predicted Peak Cooling Demand (24h)')
    plt.legend(['Real','Predicted'])
    timestr = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    plt.savefig("predvsreal_time_" + timestr + ".pdf")
    print('wooo')




def animate_voting(votes,y_plot):
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import matplotlib.animation as manimation

    metadata = dict(title='Movie Test', artist='Matplotlib', comment='Movie support!')
    FFMpegWriter = manimation.writers['ffmpeg']
    writer_anim = FFMpegWriter(fps=10, metadata=metadata)

    fig = plt.figure()
    with writer_anim.saving(fig, 'test.mp4', dpi=100):
        for i in range(len(votes)):

            ax1 = fig.add_subplot(121)
            ax1.set_ylim([0, max(votes)])
            ax1.bar(1,votes[i], 15, color='b')
            ax2 = fig.add_subplot(122)
            ax2.set_ylim([0, 3000])
            ax2.plot(np.ones(len(y_plot[i,:]))*1200,'k')
            ax2.plot(y_plot[i,:], 'r')
            writer_anim.grab_frame()
            ax1.remove()
            ax2.remove()
            print("Progress %d" %(i/len(votes)*100))

        writer_anim.finish()


def plot_voting(votes,y_plot,y_plot2):
    import matplotlib.pyplot as plt
    x=np.arange(len(votes))
    y_max = np.max(y_plot,1)
    no_hours = np.sum(y_plot>1200,1)
    y_lim = np.ones(len(votes))*1200

    fig, axes = plt.subplots(nrows=2, ncols=1)
    l1 = axes[0].plot(y_max,'r-', linewidth=1.5)
    l2 = axes[0].plot(y_lim ,'k--', linewidth=1)
    axes[0].fill_between(x, y_max, y_lim ,  where= y_max >= y_lim ,facecolors='red')
    axes[0].set_ylim([0,3000])
    ax2=axes[0].twinx()
    ax2.plot(y_plot2[:,1],'g-', linewidth=1.5)


    axes[1].bar(x, votes, 10, color='b')
    axes[1].set_ylim([0,max(votes)])
    fig.tight_layout()
    plt.show()
    print(len(votes))
    print(len(y_plot))


def heatmap(data_vec_pd,xlabel):
    import seaborn as sns
    import matplotlib.pyplot as plt
    pal = sns.color_palette("coolwarm", 28)
    sns.set(font_scale=1.4)
    # calculate the correlation matrix
    corr = data_vec_pd.corr()

    # plot the heatmap
    sns.heatmap(corr,vmin=-1,vmax=1,
                xticklabels=xlabel,
                yticklabels=xlabel,cmap=pal)
    plt.yticks(rotation=0)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig('heatmap.pdf')
    plt.show()


