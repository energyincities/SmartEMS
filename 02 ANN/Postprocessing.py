# This file computes how well the predictive model helps to optimise the scheduling of the second chiller

import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt

# load in data set
load = False
if load == True:
    data = pd.read_excel('output_incl_coverage.xlsx')
    data.to_hdf('data', 'w')
else:
    data = pd.read_hdf('data')


data_select = range(7800, 16500)
data = data.ix[data_select, 5:13]

# Load data
yhat = np.load('yhat_data.npy')
ydata = np.load('y_data.npy')

##########################################################
##########################################################
# Compute R^2 score
r2 = r2_score(ydata,yhat)
print('R^2 score: %.2f' %r2)
##########################################################
##########################################################




bl=1100
# compute number of hours the human took the wrong decision
bin = (data['SumCh3Ch4']>bl)
print('Number of hours both chillers are required:')
print(sum(bin))

bin=(data['Co-operation']>=0.5)
print('Number of hours both chillers were actually running:')
print(sum(bin))

bin=(data['Co-operation']>=0.5)&(data['SumCh3Ch4']>bl)
print('Number of hours both chillers were running in the right moment (rest only second chiller):')
print(sum(bin))

bin=(data['Co-operation']>=0.5)&(data['SumCh3Ch4']<bl)
print('Number of hours both chillers were running in the wrong moment:')
print(sum(bin))

bin=(data['Co-operation']<0.5)&(data['SumCh3Ch4']>bl)&(data['SumCh3Ch4']<1200)
print('Number of hours chiller A running when both should be running:')
print(sum(bin))



# compute number of days the human took the wrong decision
prob=0.5
error=yhat-ydata
from scipy.stats import norm
b=1200-yhat
z_b=(b-np.mean(error))/np.std(error)
final1 = 1-norm.cdf(z_b)
print("Number of hours predicted both chillers to be on (Prob: 60%)")
print(sum(final1>prob))

b=1200-yhat
z_b=(b-np.mean(error))/np.std(error)
final2 = norm.cdf(z_b)
print("Number of hours predicted only one chillers to be on (Prob: 60%)")
print(sum(final2>prob))

# Compute share of given predictions
print("Share of samples to be considered:")
print(sum(((final2>prob)|(final1>prob))/len(yhat))*100)

print("lala")






bin=(yhat>1200)&(data['SumCh3Ch4']>bl)
print('Number of hours both chillers are predicted to run in the right moment:')
print(sum(bin))

bin=(yhat<1200)&(data['SumCh3Ch4']<bl)
print('Number of hours only one chiller is running in the right moment:')
print(sum(bin))

bin=(yhat>1200)&(data['SumCh3Ch4']<bl)
print('Number of hours both chillers are predicted to run when they should not be running:')
print(sum(bin))

bin=(yhat<1200)&(data['SumCh3Ch4']>bl)
print('Number of hours when only one chiller is predicted to run although two should be running:')
print(sum(bin))
# compare to predictions


