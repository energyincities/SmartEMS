import matplotlib.pyplot as plt
import numpy as np


####################################################################################################
#1) Real vs Predicted
#Load data
y_test=np.load('y_test.npy')
yhat=np.load('yhat.npy')

#plt.rc('text', usetex=True, fontsize=10)

fig = plt.figure(figsize=[10,5])
plt.plot(y_test,'b')
plt.plot(yhat,'r')
#plt.legend(['Real','Predicted'])

plt.show()