from __future__ import print_function


import numpy as np
import pandas as pd

from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
from keras import regularizers
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

import functions as fct
import time



### Import Data
df_weather = pd.read_excel('output_incl_coverage.xlsx')
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)

####################################################################
##################### Helper Functions #############################
####################################################################
from keras import backend as K

def r2_keras(y_true, y_pred):
    SS_res =  K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )



def hyper_opt(parset):
    """"
    Function for hyperparameter optimisation
    """




class parset:
    """
    Class which defines the set of parameters used for training
    """
    def __init__(self, no_epochs, size_batch,lossfct):
        self.epochs=no_epochs
        self.batch_size=size_batch
        self.loss = lossfct

    def print(self):
        print('Epochs:%d' % self.epochs)
        print('BatchSize:%d' % self.size_batch)
        print('Loss Function used: ' + self.loss)



####################################################################
##################### KERAS API ASSEMBLY ###########################
####################################################################

# Preprocess data

inputs_weather=["temp","humid","wind","dew_point","cloud_cover_8","visibility","Saturday","Sunday","Steam","Elevation","Azimuth"]

df_weather, inputs_l, inputs_p,outputs_p = fct.pred_lag_Features(df_weather, tau=1, inputs_l=inputs_weather)
outputs_p = ['SumCh3Ch4']

data_select = range(7800, 16500)

X_data = df_weather.ix[data_select,["temp","humid","wind","Steam",
                            "dew_point","cloud_cover_8","visibility",
                            "Hour_1","Hour_2","Hour_3","Hour_4","Hour_5","Hour_6",
                            "Hour_7","Hour_8","Hour_9","Hour_10","Hour_11","Hour_12",
                            "Hour_13","Hour_14","Hour_15","Hour_16","Hour_17","Hour_18",
                            "Hour_19","Hour_20","Hour_21","Hour_22","Hour_23","Hour_0",
                            "Saturday","Sunday","Elevation","Azimuth","ChillersOff","FixControl",
                            ]+inputs_l].as_matrix()


y_data = np.reshape(df_weather.ix[data_select, outputs_p].as_matrix(),[len(X_data),len(outputs_p)])




no_feat = len(X_data[1,:]) # no. of features
print(no_feat)

# 2) Normalize data, Training-Test split
sweek_index=3800
scaler=MinMaxScaler(feature_range=(0,1))
data_norm = np.concatenate((y_data, X_data), axis=1)
data_norm = scaler.fit_transform(data_norm)

y_data = data_norm[:, 0]
X_data = data_norm[:, 1:]

[X_train, X_test, y_train, y_test] = fct.splt_data(X_data, y_data, 0.3, 'chronological')


# Reshape dataset into required format
X_data = X_data.reshape(X_data.shape[0], 1, X_data.shape[1])
X_train = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])
X_test = X_test.reshape(X_test.shape[0], 1, X_test.shape[1])

# Design and fit the model
model = Sequential()

#LSTM layer
model.add(LSTM(10, input_shape=(X_train.shape[1], X_train.shape[2]), return_sequences=True))
#model.add(Dropout(0.2))
model.add(LSTM(10))
#model.add(Dropout(0.2))

#Output layer
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam', metrics=[r2_keras])

#fit the model
start = time.time()
history = model.fit(X_train, y_train, epochs=180, batch_size=36, validation_data=(X_test, y_test), verbose=2, shuffle=False)
print('training time: ', time.time()- start)

# plot history
#plt.figure()
#plt.plot(history.history['loss'], label='train')
#plt.plot(history.history['val_loss'], label='test')
#plt.legend()
#plt.show()

# make a prediction on test set
yhat = model.predict(X_test)
X_test = X_test.reshape((X_test.shape[0], 1*no_feat))
# Denormalise predictions
data_denorm = np.concatenate((yhat.reshape(len(yhat),1), X_test), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
yhat = data_denorm[:,0]

data_denorm = np.concatenate((y_test.reshape(len(y_test),1), X_test), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
y_test = data_denorm[:,0]



np.save('yhat', yhat)
np.save('y_test',y_test)

# make a prediction on training set
yhat_train = model.predict(X_train)
X_train = X_train.reshape((X_train.shape[0], 1*no_feat))
# Denormalise predictions
data_denorm = np.concatenate((yhat_train.reshape(len(yhat_train),1), X_train), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
yhat_train = data_denorm[:,0]

data_denorm = np.concatenate((y_train.reshape(len(y_train),1), X_train), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
y_train = data_denorm[:,0]



np.save('yhat_train', yhat_train)
np.save('y_train',y_train)

##############################################################################
# make a prediction on whole set
yhat_data = model.predict(X_data)
X_data = X_data.reshape((X_data.shape[0], 1*no_feat))
# Denormalise predictions
data_denorm = np.concatenate((yhat_data.reshape(len(yhat_data),1), X_data), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
yhat_data = data_denorm[:,0]

data_denorm = np.concatenate((y_data.reshape(len(y_data),1), X_data), axis=1)
data_denorm = scaler.inverse_transform(data_denorm)
y_data = data_denorm[:,0]



np.save('yhat_data', yhat_data)
np.save('y_data', y_data)
##############################################################################

# calculate RMSE
rmse = np.sqrt(mean_squared_error(y_test, yhat))
print('Test RMSE: %.3f' % rmse)

# calculate R^2
r2 = r2_score(y_test,yhat)
print('Test R^2: %.3f' % r2)

r2 = r2_score(y_train,yhat_train)
print('Train R^2: %.3f' % r2)