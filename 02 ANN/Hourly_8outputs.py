from __future__ import print_function


import tensorflow as tf
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import functions as fct
import numpy as np



### Import Data
df_weather = pd.read_excel('output_incl_coverage.xlsx')
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)

####################################################################
##################### Helper Functions #############################
####################################################################
def get_save_path(net_number):
    return "checkpoint/network"+ str(net_number)


def optimize(training_epochs, display_step, X_train, y_train, X_test, y_test, reg_par=None, lr_par=None):
    sess.run(init_op)
    fig = plt.figure()
    for i in range (training_epochs):

        if (reg_par==None)& (lr_par==None):       # Check if hyperparameter optimisation happening
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train})
        elif (reg_par!=None)& (lr_par==None):
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train, alpha: reg_par})
        elif (reg_par==None)& (lr_par!=None):
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train, beta: lr_par})
        else:
            sess.run([train_op], feed_dict={X: X_train, Y: y_train, alpha: reg_par, beta: lr_par})

        if i % display_step == 0:
            #print(i)
            pred = sess.run(R_squared, feed_dict={X: X_test, Y: y_test})
            plt.plot(i, pred, 'bx')
            pred = sess.run(R_squared, feed_dict={X: X_train, Y: y_train})
            plt.plot(i, pred, 'rx')

            # create summary
            #result = sess.run(merged, feed_dict={X: X_train, Y: y_train, alpha: reg_par, beta: lr_par})
            #writer.add_summary(result, i)
            plt.pause(0.1)
    print("Accuracy of Network")
    pred_test = sess.run(R_squared, feed_dict={X: X_test, Y: y_test})
    print(pred_test)
    plt.close()
    return


def ensemble_predictions(num_networks,X_test):
    # Empty list of predicted labels for each of the neural networks.
    predictions = []

    # Classification accuracy on the test-set for each network.
    #test_score = []

    # For each neural network in the ensemble.
    for i in range(num_networks):
        # Reload the variables into the TensorFlow graph.
        saver.restore(sess=sess, save_path=get_save_path(i))

        # Print Networks Accuracy

        #Predictions
        pred = sess.run(y_pred, feed_dict={X: X_test})

        # Append the predicted labels to the list.
        predictions.append(pred)
    #predictions.reshape([num_networks, len(X_test)])
    return np.array(predictions)

####################################################################
##################### GRAPH ASSEMBLY ###############################
####################################################################

# 0) Network Parameters
### Static Parameters
training_epochs =3000
display_step = 300

n_hidden_1 = 50
n_hidden_2 = 50


num_networks=5  #For ensamble predictions

### Parameters for Hyperparameter-Optimisation
# Regularization parameter
alpha = 100 # tf.placeholder(tf.float32, None, name="Alpha")
#hy_par = [0.01 , 100, 1000, 10000, 100000]

# Learning rate
beta = 0.01 #tf.placeholder(tf.float32, None, name="Beta")
#learning_rate = [0.001, 0.01, 0.1]

#alpha=250

# 1) Derive Feature Vector and Output Vector



inputs_weather=["temp","humid","wind","dew_point","cloud_cover_8","visibility","Saturday","Sunday"]

df_weather, inputs_l, inputs_p,outputs_p = fct.pred_lag_Features(df_weather, tau=1, tau_p=8, inputs_l=["SumCh3Ch4","Steam"],inputs_p=inputs_weather, outputs_p=["SumCh3Ch4"])
outputs_p= ["SumCh3Ch4"]+outputs_p
data_select = range(8300, 16000)

X_data = df_weather.ix[data_select,["temp","humid","wind",
                            "dew_point","cloud_cover_8","visibility",
                            "Hour_1.0","Hour_2.0","Hour_3.0","Hour_4.0","Hour_5.0","Hour_6.0",
                            "Hour_7.0","Hour_8.0","Hour_9.0","Hour_10.0","Hour_11.0","Hour_12.0",
                            "Hour_13.0","Hour_14.0","Hour_15.0","Hour_16.0","Hour_17.0","Hour_18.0",
                            "Hour_19.0","Hour_20.0","Hour_21.0","Hour_22.0","Hour_23.0","Hour_0.0",
                            "Saturday","Sunday",
                            ]+inputs_p+inputs_l].as_matrix()


y_data = np.reshape(df_weather.ix[data_select, outputs_p].as_matrix(),[len(X_data),len(outputs_p)])


no_feat = len(X_data[1,:]) # no. of features
print(no_feat)

# 2) Normalize data, Training-Test split
sweek_index=3800
X_norm, mean, std = fct.meanNormalization(X_data)

[X_norm, X_test, y_data, y_test] = fct.splt_data(X_norm, y_data, sweek_index, 'shoulderweek')

# 3) Define Placeholder (class definition)
X = tf.placeholder(tf.float32, [None, no_feat], name="X")
Y = tf.placeholder(tf.float32, [None,len(outputs_p)], name="y")

# 4) Define Weights, Biases (class definition)
with tf.name_scope("Variable_Definition"):
    weights = {
        'h1': tf.Variable(tf.random_normal([no_feat,n_hidden_1]),name="HiddenLayer1"),
        'h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2]),name="HiddenLayer2"),
        'out': tf.Variable(tf.random_normal([n_hidden_2, len(outputs_p)]),name="OutputLayer1")
    }

    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]),name="Bias"),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]),name="Bias"),
        'out': tf.Variable(tf.random_normal([len(outputs_p)]))
    }

# 5) Define Inference Model
with tf.name_scope("MLP_Model"):
    def multilayer_perceptron(x):
        with tf.name_scope("HL1"):
            layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
            layer_1 = tf.nn.relu(layer_1)
        with tf.name_scope("HL2"):
            layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
            layer_2 = tf.nn.relu(layer_2)
        with tf.name_scope("OutputLayer"):
            out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
        return out_layer
    # 5b) Construct Model
    y_pred = multilayer_perceptron(X)

# 6) Define Loss function (operation definition), incl. L2 Regularization
with tf.name_scope("Cost_regularized"):
    loss_op = tf.reduce_mean(tf.square(Y - y_pred)) + alpha*(\
              tf.nn.l2_loss(weights['h1'])+ \
              tf.nn.l2_loss(weights['h2'])+ \
              tf.nn.l2_loss(weights['out']))
    tf.summary.scalar("Test",loss_op)

# 6a) R^2 score (operation definition)
with tf.name_scope("R2_Score"):
    total_error = tf.reduce_sum(tf.square(Y - tf.reduce_mean(Y)))
    unexplained_error = tf.reduce_sum(tf.square(Y - y_pred))
    R_squared = tf.subtract(1.0, tf.div(unexplained_error,total_error))
    tf.summary.scalar("R2",R_squared)

# 6a) R^2 score multi-output
with tf.name_scope("R2_Score_multi"):
    total_error_multi = tf.reduce_sum(tf.square(Y - tf.reduce_mean(Y,axis=0)), axis=0)
    unexplained_error_multi = tf.reduce_sum(tf.square(Y - y_pred), axis=0)
    R_squared_multi = tf.subtract(1.0, tf.div(unexplained_error_multi,total_error_multi))


# 6b) Mean absolute error
with tf.name_scope("MAE"):
    mae = tf.reduce_mean(tf.abs(Y - y_pred))
    tf.summary.scalar("MAE",loss_op)

# 7) Define Optimizer
with tf.name_scope("Training"):
    optimizer = tf.train.AdamOptimizer(learning_rate=beta,name="Training")
    train_op = optimizer.minimize(loss_op)

# 8) Define Variable initializer
init_op = tf.global_variables_initializer()

# 9) Define Saver Operation for Ensamble Methods
with tf.name_scope("Ensemble"):
    saver=tf.train.Saver(max_to_keep=100)



####################################################################
##################### GRAPH Execution ##############################
####################################################################

# set up animation


with tf.Session() as sess:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter("logs/NN", sess.graph)
    results_nets_test = pd.DataFrame(index=range(num_networks),columns=range(len(outputs_p)))
    results_nets_train = pd.DataFrame(index=range(num_networks), columns=range(len(outputs_p)))

    for no_net in range(num_networks):
        print("Training Network %d" % no_net)
        print("Hyperparameter alpha: %.5f"  %alpha)
        print("Learning rate beta: %.5f" %beta)
        #print("Training Network %d"  %i)
        [X_train, X_val, y_train, y_val] = fct.splt_data(X_norm, y_data, 0.1,'chronological')
        optimize(training_epochs, display_step, X_train, y_train, X_val, y_val)
        saver.save(sess=sess, save_path=get_save_path(no_net))

        rsqu_test = sess.run(R_squared_multi, feed_dict={X: X_test, Y: y_test})
        rsqu_train  = sess.run(R_squared_multi, feed_dict={X: X_train, Y: y_train})
        results_nets_test.ix[no_net,:] = rsqu_test
        results_nets_train.ix[no_net, :] = rsqu_train

            #saver.save(sess=sess, save_path=get_save_path(i))

    # Store Hyperparameter grid:

    # Output final coefficients
    if num_networks>1:
        predictions = ensemble_predictions(num_networks,X_test)
        mean_pred=predictions.mean(axis=0)
        #mean_pred = predictions.reshape([num_networks,len(X_test)]).mean(0).reshape([len(X_test), len(outputs_p)])
        rqu = sess.run(R_squared_multi,feed_dict={Y: y_test, y_pred: mean_pred})
        #maerr = sess.run(mae, feed_dict={Y: y_test, y_pred: mean_pred})
        #print("#####Accuracy of ensemble network: %.2f #########" %rqu)
        #print("#####MAE of ensemble network: %.2f #########" % maerr)
        #print("#####No of samples: %.2f #########" % len((X_test)))
        # Implement Voting
        votes = np.sum((predictions > 1200),axis=0)
        np.save('votes',votes)
    else:
        pass


    #Plots
    #fct.predplot(y_hat=mean_pred, y_test=y_test)
    writer.close()

