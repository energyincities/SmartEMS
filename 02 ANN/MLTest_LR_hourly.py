from __future__ import print_function


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import functions as fct
from sklearn.linear_model import LinearRegression
import statsmodels.api as sm


### Import Data

df_weather = pd.read_excel('output_incl_coverage.xlsx')
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)



# 1) Derive Feature Vector and Output Vector

X = df_weather.ix[7800:16500,["temp","humid","wind","Steam",
                            "dew_point","cloud_cover_8","visibility",
                            "Hour_1","Hour_2","Hour_3","Hour_4","Hour_5","Hour_6",
                            "Hour_7","Hour_8","Hour_9","Hour_10","Hour_11","Hour_12",
                            "Hour_13","Hour_14","Hour_15","Hour_16","Hour_17","Hour_18",
                            "Hour_19","Hour_20","Hour_21","Hour_22","Hour_23","Hour_0",
                            "Saturday","Sunday","Elevation","Azimuth","ChillersOff","FixControl",
                            ]].as_matrix()
y = np.reshape(df_weather.ix[7800:16500, 'SumCh3Ch4'].as_matrix(),[len(X),1])

# 2) Preprocessing

# 3) Normalize Data
# here: apply mean normalization
#X_norm, mean, std = fct.meanNormalization(X)

# 4) Split into Test and Training Set
[X_train, X_test, y_train, y_test] = fct.splt_data(X, y, 0.3, 'chronological')

# 5) Learning (including cross validation)


reg = LinearRegression()
reg.fit(X_train,
        y_train)




# 6) Make Predictions
y_hat=reg.predict(X_test)
y_hat_train=reg.predict(X_train)


# 7) Compute Performance
# R^2 score
score_test = reg.score(X_test,
                       y_test)
str = 'Test R^2 score:%f' %score_test
print(str)


score_train = reg.score(X_train,
                        y_train)
str = 'Train R^2 score:%f' %score_train
print(str)

# error + std

error_test = np.mean(y_test - y_hat)
std_test = np.std(y_test - y_hat)
print(error_test)
print(std_test)

print(reg.coef_)

X_train2 = sm.add_constant(X_train)
est = sm.OLS(y_train,X_train2)
est2 = est.fit()
results= est2.summary()
print(results)

