import pandas
import pickle

from itertools import chain, combinations
import datetime

## Feature extraction

#time conversions (these will mutate the DF)
def encodeTime(df, col = 'time'):
    """Convert columns with a textural representation to a datetime dataformat"""
    df[col] = pandas.to_datetime(df[col])

def breakTime(df, dateParts = 'YMDhms', col = 'time'):
    """Separates a datetime column into several componnents such as month, day and hour"""
    abbreviations = {'Y': 'year', 'M': 'month', 'D': 'day',
                     'h': 'hour', 'm' : 'minute','s': 'second'}
    effects = {'year': df[col].dt.year, 'month' : df[col].dt.month,
               'day' : df[col].dt.day, 'hour' : df[col].dt.hour,
               'minute' : df[col].dt.minute, 'second' : df[col].dt.second}
    for dp in dateParts:
        part = abbreviations.get(dp,dp)
        df[part] = effects[part]
        
#mutates the df
def categoryBucket(df,col,buckets,catGroups=None,groupName=None):
    """Groups sets of values into a bucket (i.e. hours->morning,afternoon,evening)
    
    df = pandas dataframe with to modify
    col = the column from which the original values are taken
    buckets = a dictionary mapping bucket names to lists of values to put in that bucket
    catGroups = a dictionary so that all buckets created by a call to this function can be treated as a group
    
    (Untested) This function supports values fitting in more than one bucket.
    """
    test = lambda cat: (df[col] == cat)
    for bucket,labels in buckets.items():
        df[bucket] = 0
        for label in labels:
            df[bucket] |= test(label)
    if catGroups or groupName:
        assert catGroups and groupName, 'catGroups and groupName must be provided together'
        catGroups[groupName] = list(buckets.keys())   

def oneHot(data,columns):
    """Returns a new dataframe with extra columns corresponding to the one-hot-encoded variables"""
    df = pandas.get_dummies(data.loc[:,columns],columns=columns)

    catGroups = {c: ['%s_%s' %(c,val) for val in data[c].dropna().drop_duplicates()]
                 for c in columns}
    return data.merge(df,on=df.index), catGroups

def discretize(data,columns,bins,precision=50):
    """Converts several columns of continuous data to discrete data by bucketing into equal numbers of datapoints
    
    Precision helps to prevent excessively long bucket labels, but is only partially successfull."""
    #note that all null columns will cause errors
    #and columns with few distinct values may do so as well
    for column in columns:
        #the precision value reduces, but does not totally prevent long bucket labe
        data[column] = pandas.qcut(data[column], bins, duplicates = 'drop',precision=precision)

## Rule Generation
    
#WARNING: duplicated column names are an issue, as row[col] will retrieve more than one item
def rowToList(df):
    """Produces a helper function to be used with df.apply().
    That function will convert a row of boolean values to a list of transactions.
    
    The list of transactions will contain the column indexes of all columns that have a true value.
    This function cannont handle multiple columns with the same name"""
    cols = list(df.columns)
    def _rowToList(row):
        for col in cols:
            return tuple([cols.index(col) for col in cols if row[col]])
    return _rowToList

def powerSet(iterable):
    """Returns a generator that contains all subsets of 'iterable'.
    This excludes the empty set and the entire input set."""
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)))

#confidence/lift constraints could be moved here
def candidateRules(freqSets, sources, targets, total, min_improvement=0):
    """Generates a dataframe of all rules that can be derived from the frequent sets.
    Each rule also has the total support of the componnents stored as well."""
    rules = []
    freqSets[frozenset()] = total
    for target in targets:
        #skip infrequent consequents
        if frozenset([target]) not in freqSets:
            continue
        by = bestYet(freqSets, frozenset([target]), {frozenset() : freqSets[frozenset([target])]/total}) 
        #the following two lines could be improved with a tree structure to deal with subsets
        for antecedent in freqSets:
            if target not in antecedent:
                continue
            antecedent = antecedent - frozenset([target])
            if not antecedent <= sources:
                continue
            tx = freqSets[antecedent]
            ty = freqSets[frozenset([target])]
            txy = freqSets[antecedent | frozenset([target])]
            confidence = txy/tx
            newRule = {'x': antecedent, 'y': target,'total X': tx, 'total Y': ty, 'total XY': txy,
            'confidence':confidence, 'other conf': by(antecedent)}
            if not(newRule['confidence'] - newRule['other conf'] <= min_improvement):
                rules.append(newRule)
    return pandas.DataFrame(rules)

def bestYet(freqSets, target, cache):
    """Helper function used by candidateRules
    
    Recursively Generates the best confidence value for subkeys of a given key.(target)"""
    #python has a builtin for caching. Consider swapping that in for the current method.
    def by(key):
        if cache.get(key) != None:
            return cache[key]
        cache[key] = max(*(cache.get(frozenset(subkey), by(frozenset(subkey)))
                        for subkey in combinations(key,len(key)-1)),
                      *(freqSets[frozenset(subkey) | target]/freqSets[frozenset(subkey)] 
                        for subkey in combinations(key,len(key)-1)))
        return cache[key]
    return by


## Rule Filtering
def liftConf(df, total):
    """mutates ruleDF to include filtering criteria"""
    for name in ['X','Y','XY']:
        df['support %s' % name] = df['total %s' % name]/total

    df['confidence'] = df['support XY']/df['support X']
    df['lift'] = df['support XY']/(df['support X'] * df['support Y'])
    return df

## Rule Display

def rulesFor(ruleDF, name, predictive=True, maxFound = 6):
    """Generates a string displaying up to `maxFound` rules that predict or are predicted by `name`
    If predictive is false, then rules that are predictied by `name` will be output. This acts as a descriptive mode"""
    if predictive:
        toFind = 'x'
        fixed = 'y'
        t = '{result}\n==> {name}'
    else:
        toFind = 'y'
        fixed = 'x'
        t = '{name} ==>\n{result}'
    rules = ruleDF[ruleDF[fixed]==frozenset([name])].sort_values(['confidence'],ascending = False).iterrows()
    result = '\n'.join([', '.join(row[toFind]) for index,row in islice(rules, maxFound)])
    if not result:
        result = 'Nothing'
    return t.format(name=name, result=result)

## Misc
def expandGroups(catGroups, *names):
    """Converts category group names to the corresponding binary column names.
    
    catGroups = a mapping from group names to a list of the commponnent columns
    names = the group names to expand. If absent, all groups will be used.
    """
    if names:
        return set(chain(*[catGroups.get(group, [group]) for group in names]))
    else:
        return set(chain(*catGroups.values()))

def simpleTime(time=True):
    """Prints how long a function took to run.
    Not as robust as the timit module, this is for ballpark timing not benchmarking.
    """
    def decorate(func):
        if time:
            def timedFunc(*args, **kwargs):
                start = datetime.datetime.now()
                result = func(*args,**kwargs)
                end = datetime.datetime.now()
                print(end-start)
                return result
            return timedFunc
        else:
            return func
    return decorate