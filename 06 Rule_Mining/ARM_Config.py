SourceDataPath = '../05 WeatherData/output_incl_coverage.xlsx'
OutputDataDir = 'data/'

categoryGroups = OutputDataDir + 'categoryGroups.p'
frequentSets = OutputDataDir + 'frequentSets.p'
features = OutputDataDir + 'features.p'
rules_unfiltered = OutputDataDir + r'/ruleDF-unfiltered'
rules_filtered = OutputDataDir + r'/ruleDF'


TIME_NAMES = ['year', 'month', 'day','hour']
CATEGORY_NAMES = ['cond_str', 'cluster', 'clouds', 'fog', 'rain', 'snow',] + TIME_NAMES
SEASONS = {'Winter':[12,1,2],
           'Fall':[9,10,11],
           'Spring':[3,4,5],
           'Summer':[6,7,8]}
DAYGROUPS = {'Morning':list(range(5,10)),
           'Midday':list(range(10,18)),
           'Evening':list(range(18,22)),
           'Night':list(range(22,24))+list(range(0,5))}
TO_BIN = ['temp', 'humid', 'wind', 'dew_point',
          'wind_speed', 'wind_gust', 'relative_humidity',
          'windchill', 'humidex', 'visibility', 
          'Elevation', 'Azimuth', 'DHI', 'DNI', 'Pressure']