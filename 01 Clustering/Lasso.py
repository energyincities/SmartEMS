from __future__ import print_function


import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import functions as fct
from sklearn import linear_model


### Import Data
df_weather = pd.read_excel('WeatherData/output_incl_coverage.xlsx')
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["cluster_steam"],prefix="cluster")],axis=1)
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["month"],prefix="Month")],axis=1)

### Select in- and outputs
inputs = ["temp","humid","wind","Steam",#"dew_point",
        "cloud_cover_8","fog","visibility",
        "Hour_1","Hour_2","Hour_3","Hour_4","Hour_5","Hour_6",
        "Hour_7","Hour_8","Hour_9","Hour_10","Hour_11","Hour_12",
        "Hour_13","Hour_14","Hour_15","Hour_16","Hour_17","Hour_18",
        "Hour_19","Hour_20","Hour_21","Hour_22","Hour_23","Hour_0",
        "Saturday","Sunday",
        ]


### General
X_data = df_weather.ix[3000:16000, inputs].as_matrix()
y_data = np.reshape(df_weather.ix[3000:16000, 'SumCh3Ch4'].as_matrix(), [len(X_data), 1])

X_data, mean, std = fct.meanNormalization(X_data)

### Apply Lasso regression
clf = linear_model.Lasso(alpha=0.0001)
clf.fit(X_data, y_data)

print(clf.score(X_data, y_data))
print(clf.coef_)
inputs_str = [i.replace("_", " ") for i in inputs]
print(inputs_str)
plt.plot(np.arange(len(clf.coef_)), clf.coef_, 'x')
plt.xticks(np.arange(len(clf.coef_)), inputs_str, rotation=90)
plt.show()
plt.pause(1.0)



### Clusterwise
for cluster in ["cluster_0.0","cluster_1.0"]:
    ind = (df_weather.ix[3000:16000, cluster] == 1)

    X_data = df_weather.ix[3000:16000, inputs].as_matrix()
    y_data = np.reshape(df_weather.ix[3000:16000, 'SumCh3Ch4'].as_matrix(), [len(X_data), 1])

    X_data = X_data[ind, :]
    y_data = y_data[ind, :]

    X_data, mean, std = fct.meanNormalization(X_data)

    ### Apply Lasso regression
    clf = linear_model.Lasso(alpha=0.0005)
    clf.fit(X_data,y_data)

    print(clf.score(X_data,y_data))
    print(clf.coef_)
    inputs_str=[i.replace("_"," ")for i in inputs]
    print(inputs_str)
    plt.plot(np.arange(len(clf.coef_)),clf.coef_,'x')
    plt.xticks(np.arange(len(clf.coef_)),inputs_str,rotation=90)
    plt.show()
    plt.pause(1.0)


