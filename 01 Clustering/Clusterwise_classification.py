from __future__ import print_function


import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import functions as fct
import numpy as np



### Import Data
df_weather = pd.read_excel('WeatherData/output_incl_coverage.xlsx')
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["cluster"],prefix="cluster")],axis=1)
df_weather = pd.concat([df_weather,pd.get_dummies(df_weather["month"],prefix="Month")],axis=1)
### collect previous cooling demand data
tau=24
for i in range(1, tau):
    mystr = '_lagged%.0d' %i
    df_weather = df_weather.join(df_weather['SumCh3Ch4'].shift(i), rsuffix=mystr)
del i

df_weather=df_weather.ix[tau:,:]

####################################################################
##################### Helper Functions #############################
####################################################################
def get_save_path(net_number):
    return "checkpoint/network"+ str(net_number)


def optimize(training_epochs, display_step, X_train, y_train, X_test, y_test, reg_par=None):
    sess.run(init_op)
    fig = plt.figure()
    for i in range (training_epochs):

        if reg_par==None:       # Check if hyperparameter optimisation happening
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train})
        else:
            sess.run([train_op], feed_dict={X: X_train, Y: y_train, alpha: reg_par})

        if i % display_step == 0:
            #print(i)
            pred = sess.run(loss_op, feed_dict={X: X_test, Y: y_test})
            plt.plot(i, pred, 'bx')
            pred = sess.run(loss_op, feed_dict={X: X_train, Y: y_train})
            plt.plot(i, pred, 'rx')

            # create summary
            #result = sess.run(merged, feed_dict={X: X_train, Y: y_train, alpha: reg_par})
            #writer.add_summary(result, i)
            plt.pause(0.1)
    print("Accuracy of Network")
    plt.close()
    return


def ensemble_predictions(num_networks,X_test):
    # Empty list of predicted labels for each of the neural networks.
    predictions = []

    # Classification accuracy on the test-set for each network.
    #test_score = []

    # For each neural network in the ensemble.
    for i in range(num_networks):
        # Reload the variables into the TensorFlow graph.
        saver.restore(sess=sess, save_path=get_save_path(i))

        # Print Networks Accuracy

        #Predictions
        pred = sess.run(y_pred, feed_dict={X: X_test})

        # Append the predicted labels to the list.
        predictions.append(pred)
    #predictions.reshape([num_networks, len(X_test)])
    return np.array(predictions)

####################################################################
##################### GRAPH ASSEMBLY ###############################
####################################################################

# 0) Network Parameters
### Static Parameters
learning_rate = 0.9
training_epochs =1000
display_step = 100

n_hidden_1 = 30
n_hidden_2 = 30

num_networks=1  #For ensamble predictions

### Parameters for Hyperparameter-Optimisation

alpha = tf.placeholder(tf.float32, None, name="Alpha")


# 1) Derive Feature Vector and Output Vector

X_data = df_weather.ix[0:16000,["temp","humid","wind","Steam",
                            "dew_point","cloud_cover_8","fog","visibility",
                            "Hour_1","Hour_2","Hour_3","Hour_4","Hour_5","Hour_6",
                            "Hour_7","Hour_8","Hour_9","Hour_10","Hour_11","Hour_12",
                            "Hour_13","Hour_14","Hour_15","Hour_16","Hour_17","Hour_18",
                            "Hour_19","Hour_20","Hour_21","Hour_22","Hour_23","Hour_0",
                            "Saturday","Sunday",
                            "Month_1", "Month_2", "Month_3", "Month_4", "Month_5", "Month_6",
                            "Month_7", "Month_8", "Month_9", "Month_10", "Month_11", "Month_12"
                            ]].as_matrix()


y_data = np.reshape(df_weather.ix[0:16000, ["cluster_0.0","cluster_1.0","cluster_2.0","cluster_3.0","cluster_4.0"]].as_matrix(),[len(X_data),5])


no_feat = len(X_data[1,:]) # no. of features
no_class = len(y_data[1,:]) # no. of classes (clusters)

# 2) Normalize data, Training-Test split
X_norm, mean, std = fct.meanNormalization(X_data)
[X_norm, X_test, y_data, y_test] = fct.splt_data(X_norm, y_data, 0.1)



# 3) Define Placeholder (class definition)
X = tf.placeholder(tf.float32, [None, no_feat], name="X")
Y = tf.placeholder(tf.float32, [None,no_class], name="y")

# 4) Define Weights, Biases (class definition)
# with tf.name_scope("Variable_Definition"):
#     weights = {
#         'h1': tf.Variable(tf.random_normal([no_feat,n_hidden_1]),name="HiddenLayer1"),
#         'h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2]),name="HiddenLayer2"),
#         'out': tf.Variable(tf.random_normal([n_hidden_2, no_class]),name="OutputLayer1")
#     }
#
#     biases = {
#         'b1': tf.Variable(tf.random_normal([n_hidden_1]),name="Bias"),
#         'b2': tf.Variable(tf.random_normal([n_hidden_2]),name="Bias"),
#         'out': tf.Variable(tf.random_normal([no_class]))
#     }

# 5) Define Inference Model
# with tf.name_scope("MLP_Model"):
#     def multilayer_perceptron(x):
#         with tf.name_scope("HL1"):
#             layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
#             layer_1 = tf.nn.relu(layer_1)
#         with tf.name_scope("HL2"):
#             layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
#             layer_2 = tf.nn.relu(layer_2)
#         with tf.name_scope("OutputLayer"):
#             out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
#             out_layer = tf.nn.softmax(out_layer,)
#         return out_layer
#     # 5b) Construct Model
#     y_pred = multilayer_perceptron(X)

# Corresponding to tutorial code:
Weights = tf.Variable(tf.random_normal([no_feat, no_class]))
biases = tf.Variable(tf.zeros([1, no_class]) + 0.1,)
Wx_plus_b = tf.matmul(X, Weights) + biases
y_pred = tf.nn.softmax(Wx_plus_b,)

# 6) Define Loss function (operation definition), incl. L2 Regularization
with tf.name_scope("CrossEntropy"):
    loss_op = tf.reduce_mean(-tf.reduce_sum(Y*tf.log(y_pred),1))
    tf.summary.scalar("Test",loss_op)

# 6a) R^2 score (operation definition)
# with tf.name_scope("R2_Score"):
#     total_error = tf.reduce_sum(tf.square(Y - tf.reduce_mean(Y)))
#     unexplained_error = tf.reduce_sum(tf.square(Y - y_pred))
#     R_squared = tf.subtract(1.0, tf.div(unexplained_error,total_error))
#     tf.summary.scalar("R2",R_squared)

# 6b) Mean absolute error
# with tf.name_scope("MAE"):
#     mae = tf.reduce_mean(tf.abs(Y - y_pred))
#     tf.summary.scalar("MAE",loss_op)

# 7) Define Optimizer
with tf.name_scope("Training"):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate,name="Training")
    train_op = optimizer.minimize(loss_op)

# 8) Define Variable initializer
init_op = tf.global_variables_initializer()

# 9) Define Saver Operation for Ensamble Methods
with tf.name_scope("Ensemble"):
    saver=tf.train.Saver(max_to_keep=100)



####################################################################
##################### GRAPH Execution ##############################
####################################################################



with tf.Session() as sess:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter("logs/NN", sess.graph)

    for i in range (num_networks):
        print("Training Network %d"  %i)
        [X_train, X_val, y_train, y_val] = fct.splt_data(X_norm, y_data, 0.1)
        optimize(training_epochs, display_step, X_train, y_train,
                 X_val, y_val)
        saver.save(sess=sess, save_path=get_save_path(i))
    # Output final predictions
    predictions = ensemble_predictions(num_networks,X_test)
    #mean_pred = predictions.reshape([num_networks,len(X_test)]).mean(0).reshape([len(X_test),no_class])
    print("#####No of samples: %.2f #########" % len((X_test)))
    #Plots
    #fct.predplot(y_hat=mean_pred, y_test=y_test)
    writer.close()

