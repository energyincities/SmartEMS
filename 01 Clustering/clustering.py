from __future__ import print_function


import tensorflow as tf
import pandas as pd

import functions as fct
import numpy as np

from scipy import linalg
import matplotlib as mpl
import matplotlib.pyplot as plt

from sklearn import mixture


### Import Data
df_weather = pd.read_excel('WeatherData/output_incl_coverage.xlsx')

import itertools
color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold',
                              'darkorange','green','k','r','y','b'])


def plot_results(X, Y_, means, covariances, index, title):
    splot = plt.subplot(2, 1, 1 + index)
    for i, (mean, covar, color) in enumerate(zip(means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], .8, color=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)

    #plt.xlim(-9., 5.)
    #plt.ylim(-3., 6.)
    plt.xlabel(r'Outside Temperature [$C^o$]')
    plt.ylabel('Cooling Demand [t]')

    plt.title(title)
    plt.savefig('clustering.pdf')




X = df_weather.ix[0:16000, ['Steam', 'SumCh3Ch4']].as_matrix()


# Fit a Gaussian mixture with EM using five components
gmm = mixture.GaussianMixture(n_components=2, covariance_type='full').fit(X)
plot_results(X, gmm.predict(X), gmm.means_, gmm.covariances_, 0,
             'Gaussian Mixture')
plot_results(X, gmm.predict(X), gmm.means_, gmm.covariances_, 0,
             'Gaussian Mixture')

# Fit a Dirichlet process Gaussian mixture using five components
#dpgmm = mixture.BayesianGaussianMixture(n_components=7,
                                        #covariance_type='full').fit(X)
#plot_results(X, dpgmm.predict(X), dpgmm.means_, dpgmm.covariances_, 1,
#             'Bayesian Gaussian Mixture with a Dirichlet process prior')

plt.show()

np.savetxt('clusters.txt',gmm.predict(X),fmt='%.1d')


###########################################################

# Apply elbow method:
min_c=1
max_c=10
# create new plot and data
plt.plot()

# determine k
bic_score = []
for k in range(min_c, max_c):
    gmm = mixture.GaussianMixture(n_components=k, covariance_type='full').fit(X)
    #kmeanModel.fit(X)
    bic_score.append(gmm.bic(X))

# Plot the elbow
plt.plot(range(min_c, max_c), bic_score, 'bx-')
plt.xlabel('k')
plt.ylabel('Bayesian Information Criterion')
plt.title('The Elbow Method showing the optimal k')
plt.show()


