#Import statements first of course:
#Note, all but tensorflow may be unneccesary right now
from __future__ import absolute_import
from __future__ import division
from __future__ import  print_function
import os
import urllib
import argparse
import sys
import tempfile

import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib

matplotlib.use('TKAgg')
from matplotlib import pyplot as plt

#Implement this
def generate_dataset():
    filename_queue = tf.train.string_input_producer(["test2.csv"])

    reader = tf.TextLineReader()
    key, value = reader.read(filename_queue)

    record_defaults = [[1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0],
                       [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0],
                       [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0],
                       [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0],
                       [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0],
                       [1.0], [1.0], [1.0], [1.0], [1.0], [1.0]]
    col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72, col73, col74, col75, col76 = tf.decode_csv(
        value, record_defaults=record_defaults)
    features = tf.stack(
        [col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18,
         col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34,
         col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50,
         col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66,
         col67, col68, col69, col70, col71, col72, col73])


    y = [2]
    x = features
    return x, y

def regression():
    x = tf.placeholder(tf.float32, shape=(None,), name='x')
    y = tf.placeholder(tf.float32, shape=(None,), name='y')

    with tf.variable_scope('lreg') as scope:
        w = tf.Variable(np.random.normal(), name='w')
        y_predicted = tf.multiply(w, x)

        loss = tf.reduce_mean(tf.square(y_predicted - y))

    return x, y, y_predicted, loss

def run():
    x_batch, y_batch = generate_dataset()

    x, y, y_predicted, loss = regression()
    optimizer = tf.train.GradientDescentOptimizer(0.1).minimize(loss)

    init = tf.global_variables_initializer()

    with tf.Session() as session:
        session.run(init)

        feed_dict = {x: x_batch, y: y_batch}
        for _ in range(30):
            loss_val = session.run([loss, optimizer], feed_dict)
            print('loss:', loss_val.mean())  #This line might need tweaking.

        y_predicted_batch = session.run(y_predicted, {x: x_batch})



if __name__ == "__main__":
    run()