#Import statements first of course:
#Note, all but tensorflow may be unneccesary right now
from __future__ import absolute_import
from __future__ import division
from __future__ import  print_function
import os
import urllib
import argparse
import sys
import tempfile

import pandas as pd
import numpy as np
import tensorflow as tf

FLAGS = None

def deepnn(x):
    with tf.name_scope('reshape'):
        x_input = tf.reshape([-1, 72])

    with tf.name_scope('layer1'):
        W_layer1 = weight_variable([-1, 72])
        b_layer1 = bias_variable([1])
        h_layer1 = tf.nn.relu(mat_mul(x_input, W_layer1) + b_layer1)

        





def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def mat_mul(x, W):
    return tf.matmul(x, W)



def main(_):
    #This line must change!  one_hot is absolutely NOT True!
    #mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

    #Model below:
    x = tf.placeholder(tf.float32, [None, 72])