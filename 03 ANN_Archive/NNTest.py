#Import statements first of course:
#Note, all but tensorflow may be unneccesary right now
from __future__ import absolute_import
from __future__ import division
from __future__ import  print_function
import argparse
import sys
import numpy

import tensorflow as tf

#TEST
#node1 = tf.constant(2.0, dtype=tf.float32)
#ph1 = tf.placeholder(tf.float32)
#var1 = tf.Variable([1], dtype=tf.float32)
#print(node1)


#COLLECT AND FORMAT INPUT
filenames = tf.train.string_input_producer(["test2.csv"])
reader = tf.TextLineReader()
key, value = reader.read(filenames)

record_defaults = [[1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0], [1.0]]
col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72, col73, col74, col75, col76 = tf.decode_csv(value, record_defaults=record_defaults)
features = tf.stack([col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20, col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40, col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58, col59, col60, col61, col62, col63, col64, col65, col66, col67, col68, col69, col70, col71, col72])
#features = tf.stack([[col1], [col2], [col3], [col4], [col5], [col6], [col7], [col8], [col9], [col10], [col11], [col12], [col13], [col14], [col15], [col16], [col17], [col18], [col19], [col20], [col21], [col22], [col23], [col24], [col25], [col26], [col27], [col28], [col29], [col30], [col31], [col32], [col33], [col34], [col35], [col36], [col37], [col38], [col39], [col40], [col41], [col42], [col43], [col44], [col45], [col46], [col47], [col48], [col49], [col50], [col51], [col52], [col53], [col54], [col55], [col56], [col57], [col58], [col59], [col60], [col61], [col62], [col63], [col64], [col65], [col66], [col67], [col68], [col69], [col70], [col71], [col72]])

#with tf.Session() as sess:
#    coord = tf.train.Coordinator()
#    threads = tf.train.start_queue_runners(coord=coord)


#CONSTRUCT
#This part should be for declaring constants, placeholders, and variables (That aren't for testing)
x = tf.placeholder(tf.float32, [None, 72])  #Old line
#x = tf.placeholder(tf.float32, [72, None])
#careful with these next two.  Uncomment when you know what the 1 does.
#Okay, second note, the 1 is a dimension.  For that purpose, I don't like it for b, but will put up
#with it for now.
W = tf.Variable(tf.zeros([72, 1]))  #Old line
#W = tf.Variable(tf.zeros([1,72]))
b = tf.Variable(tf.zeros([1]))

#Now to declare the actual model.  This model desperately needs changing for this application.
y = tf.nn.softmax(tf.matmul(x, W) + b)
y_ = tf.placeholder(tf.float32, shape=[None, 1])

#Make a better loss function!  Remember, squared deltas should equal (result - expectation)^2
squared_deltas = (y - y_)*(y - y_)
loss = tf.reduce_sum(squared_deltas)

#Don't be afraid to play with this variable.  I've seen values from 0.01 to 0.5
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

#RUN
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
#Need input here, in the form of two dicts.  1.  x, is the input.  2. y_, is the expected output
coord = tf.train.Coordinator()
with sess.as_default():
    threads = tf.train.start_queue_runners(coord=coord)

#After training, remember to add in the actual inputs and expected values.
#Should look like sess.run(train, {something: }
for i in range(100):
    data, label = sess.run([features, col72])
#    with sess.as_default():
#        data = tf.reshape(data, [-1,72]).eval()
    sess.run(train, feed_dict={x: data, y_: label})
#    sess.run(train, [features, col72])

coord.request_stop()
coord.join(threads)
#EVALUATE


#VISUALIZE