#Import statements first of course:
#Note, all but tensorflow may be unneccesary right now
from __future__ import absolute_import
from __future__ import division
from __future__ import  print_function
import os
import urllib
import argparse
import sys

import numpy as np
import tensorflow as tf

#COLLECT AND FORMAT INPUT
WEATHER_TRAINING = "test3.csv"
WEATHER_TEST = "test4.csv"

def main():
    training_set = tf.contrib.learn.datasets.base.load_csv_with_header(filename=WEATHER_TRAINING, target_dtype=np.float32, features_dtype=np.float32)
    test_set = tf.contrib.learn.datasets.base.load_csv_with_header(filename=WEATHER_TEST, target_dtype=np.float32, features_dtype=np.float32)

    #You might need to shape and redirect these functions
    feature_columns = [tf.feature_column.numeric_column("x", shape=[72])]

    classifier = tf.estimator.DNNClassifier(feature_columns=feature_columns, hidden_units=[72], n_classes=3, model_dir="/tmp/weather_model")

    train_input_fn = tf.estimator.inputs.numpy_input_fn(x={"x": np.array(training_set.data)}, y=np.array(training_set.target), num_epochs=None, shuffle=True)

    #May need to decrease steps for data size
    classifier.train(input_fn=train_input_fn, steps=2000)

    test_input_fn = tf.estimator.inputs.numpy_input_fn(x={"x": np.array(test_set.data)}, y=np.array(test_set.target), num_epochs=1, shuffle=False)

    accuracy_score = classifier.evaluate(input_fn=test_input_fn)["accuracy"]

    print("\nTest Accuracy: {0:f}\n".format(accuracy_score))

    new_samples = np.array([[6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5, 6.4, 3.2, 4.5, 1.5, 1.5, 1.5], [5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7, 5.8, 3.1, 5.0, 1.7]], dtype=np.float32)
    predict_input_fn = tf.estimator.inputs.numpy_input_fn(x={"x": new_samples}, num_epochs=1, shuffle=False)

    predictions = list(classifier.predict(input_fn=predict_input_fn))
    predicted_classes = [p["classes"] for p in predictions]

    print("New Samples, Class Predictions:  {}\n".format(predicted_classes))

if __name__ == "__main__":
    main()

#CONSTRUCT MODEL


#RUN


#EVALUATE


#VISUALIZE