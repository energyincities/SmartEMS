
from __future__ import print_function

import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import functions as fct
import numpy as np

from matplotlib import rc
rc('text', usetex=True)

### Import Data
df_weather = pd.read_csv('hourly_data.csv')
df_weather = df_weather.ix[:,  (df_weather.columns != 'Unnamed: 0')]

#df_weather_x = df_weather.iloc[:,np.r_[0:24,72:99]]
df_weather_x = df_weather
ind = df_weather_x.ix[:, 'peak_dem']>0    # exclude zero cooling demand samples
X_data = df_weather_x.ix[ind,  (df_weather_x.columns != 'peak_dem')&
                           (df_weather_x.columns != 'CDD')].as_matrix()

y_data = np.reshape(df_weather_x.ix[ind, 'peak_dem'].as_matrix(),[554,1])



####################################################################
##################### Helper Functions #############################
####################################################################
def batchData(x,y):
    #x = np.array(np.random.choice(2, total_series_length, p=[0.5, 0.5]))
    #y = np.roll(x, echo_step)
    #y[0:echo_step] = 0

    x = x.reshape((batch_size, -1))  # The first index changing slowest, subseries as rows
    y = y.reshape((batch_size, -1))
    return (x, y)

####################################################################
##################### GRAPH ASSEMBLY ###############################
####################################################################

### 0) Network Parameters
num_epochs = 100
#total_series_length = 50000 # here: given by data: ~16000 samples
truncated_backprop_length = 15
state_size = 1 #(saved from the output of the previous run)
num_classes = 2
#echo_step = 3
batch_size = 5 # read about batch sizes here:  http://www.deeplearningbook.org/contents/optimization.html
num_batches = total_series_length//batch_size//truncated_backprop_length


### 1) Adjust data corresponding to batch size
[x,y] = batchData(X_data, y_data)


### 1) Instantiate Placeholders for Data

batchX_placeholder = tf.placeholder(tf.float32, [batch_size, truncated_backprop_length])
batchY_placeholder = tf.placeholder(tf.int32, [batch_size, truncated_backprop_length])

init_state = tf.placeholder(tf.float32, [batch_size, state_size])

### 2) Create variables for weights and biases

W = tf.Variable(np.random.rand(state_size+1, state_size), dtype=tf.float32)
b = tf.Variable(np.zeros((1,state_size)), dtype=tf.float32)

W2 = tf.Variable(np.random.rand(state_size, num_classes),dtype=tf.float32)
b2 = tf.Variable(np.zeros((1,num_classes)), dtype=tf.float32)

### 3) Unpack columns

inputs_series = tf.unpack(batchX_placeholder, axis=1)
labels_series = tf.unpack(batchY_placeholder, axis=1)

### 4) Forward Pass
current_state = init_state
states_series = []

for current_input in inputs_series:
    current_input = tf.reshape(current_input, [batch_size, 1])
    input_and_state_concatenated = tf.concat(1, [current_input, current_state])  # Increasing number of columns
    next_state = tf.tanh(tf.matmul(input_and_state_concatenated, W) + b)  # Broadcasted addition
    states_series.append(next_state)
    current_state = next_state

### 5) Calculating Losses
logits_series = [tf.matmul(state, W2) + b2 for state in states_series]  # Broadcasted addition
predictions_series = [tf.nn.softmax(logits) for logits in logits_series]
losses = [tf.nn.sparse_softmax_cross_entropy_with_logits(logits, labels) for logits, labels in zip(logits_series, labels_series)]
total_loss = tf.reduce_mean(losses)
train_step = tf.train.AdagradOptimizer(0.3).minimize(total_loss)


####################################################################
##################### GRAPH Execution ##############################
####################################################################