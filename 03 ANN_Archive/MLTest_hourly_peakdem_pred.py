from __future__ import print_function


import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import functions as fct
import numpy as np



### Import Data
df_weather = pd.read_excel('WeatherData/output_incl_coverage.xlsx')


####################################################################
##################### Helper Functions #############################
####################################################################
def get_save_path(net_number):
    return "checkpoint/network"+ str(net_number)


def optimize(training_epochs, display_step, X_train, y_train, X_test, y_test, reg_par=None):
    sess.run(init_op)
    fig = plt.figure()
    for i in range (training_epochs):

        if reg_par==None:       # Check if hyperparameter optimisation happening
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train})
        else:
            sess.run([train_op], feed_dict={X: X_train, Y: y_train, alpha: reg_par})

        if i % display_step == 0:
            #print(i)
            pred = sess.run(R_squared, feed_dict={X: X_test, Y: y_test})
            plt.plot(i, pred, 'bx')
            pred = sess.run(R_squared, feed_dict={X: X_train, Y: y_train})
            plt.plot(i, pred, 'rx')

            # create summary
            #result = sess.run(merged, feed_dict={X: X_train, Y: y_train, alpha: reg_par})
            #writer.add_summary(result, i)
            plt.pause(0.1)
    print("Accuracy of Network")
    print(sess.run(R_squared, feed_dict={X: X_test, Y: y_test}))
    plt.close()
    return


def ensemble_predictions(num_networks,X_test):
    # Empty list of predicted labels for each of the neural networks.
    predictions = []

    # For each neural network in the ensemble.
    for i in range(num_networks):
        # Reload the variables into the TensorFlow graph.
        saver.restore(sess=sess, save_path=get_save_path(i))

        # Print Networks Accuracy

        #Predictions
        pred = sess.run(y_pred, feed_dict={X: X_test})

        # Append the predicted labels to the list.
        predictions.append(pred)
    #predictions.reshape([num_networks, len(X_test)])
    return np.array(predictions)

####################################################################
##################### GRAPH ASSEMBLY ###############################
####################################################################

# 0) Network Parameters
### Static Parameters
learning_rate = 0.01
training_epochs =4000
display_step = 200

n_hidden_1 = 200
n_hidden_2 = 200

num_networks=1  #For ensamble predictions
f = open('output.csv', 'w+')
f.write('-------------------------------------------\n')
f.write('------------  Network Layout  -------------\n')
f.write('-------------------------------------------\n')
f.write('Number of hidden layers:%d \n' %2)
f.write('Units hidden layer 1: %d \n' %n_hidden_1)
f.write('Units hidden layer 2: %d \n' %n_hidden_2)
f.write('-------------------------------------------\n')
### Parameters for Hyperparameter-Optimisation

alpha = tf.placeholder(tf.float32, None, name="Alpha")
#alpha=250


tau=3
tau_p=24
inputs_cand_weather = ['temp','humid','wind','dew_point','cloud_cover_8','fog','visibility']
inputs_we = ["Saturday","Sunday"]
outputs = ["SumCh3Ch4"]
df_weather_raw = df_weather
# for inputs_weather in inputs_cand_weather:
# inputs_weather = [inputs_weather]
# f.write('----------------------------------------------------\n')
# f.write('------------- Results for ' + inputs_weather[0] + '-------------\n')
# f.write('----------------------------------------------------\n')
# f.write('Lagged Variables \n')
# f.write('(Lag: Tau = '+str(tau)+'\n')
# f.write('Forecasted Variables ' + inputs_weather[0] + '\n')
# f.write('(Lag: Tau = '+str(tau)+'\n')
# f.write('-----------------------------------------------------------------\n')
# 1) Derive Feature Vector and Output Vector
df_weather = pd.concat([df_weather_raw,pd.get_dummies(df_weather["hour"],prefix="Hour")],axis=1)
df_weather = pd.concat([df_weather_raw,pd.get_dummies(df_weather["month"],prefix="Month")],axis=1)
df_weather, inputs_l, inputs_p,outputs_p = fct.pred_lag_Features(df_weather, tau=tau, tau_p=tau_p,
                                                       inputs_l=['SumCh3Ch4'],inputs_p=inputs_cand_weather,
                                                       outputs_p=outputs)
# cut off nan values select in- & outputs
inputs_hour = ["Hour_" + str(i) for i in range(0,24)]
inputs_month = ["Month_" + str(i) for i in range(0,12)]
X_data = df_weather.ix[tau:len(df_weather)-tau_p,inputs_l+inputs_p+inputs_hour+inputs_cand_weather].as_matrix()
y_data = df_weather.ix[tau:len(df_weather)-tau_p,outputs+outputs_p].as_matrix()


no_feat = len(X_data[1,:]) # no. of features
no_out = len(y_data[1,:])
print(no_feat)

# 2) Normalize data, Training-Test split
X_norm, mean, std = fct.meanNormalization(X_data)
[X_norm, X_test, y_data, y_test] = fct.splt_data(X_norm, y_data, 0.1,mode='chronological')



# 3) Define Placeholder (class definition)
X = tf.placeholder(tf.float32, [None, no_feat], name="X")
Y = tf.placeholder(tf.float32, [None,no_out], name="y")

# 4) Define Weights, Biases (class definition)
with tf.name_scope("Variable_Definition"):
    weights = {
        'h1': tf.Variable(tf.random_normal([no_feat,n_hidden_1]),name="HiddenLayer1"),
        'h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2]),name="HiddenLayer2"),
        'out': tf.Variable(tf.random_normal([n_hidden_2, no_out]),name="OutputLayer1")
    }

    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]),name="Bias"),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]),name="Bias"),
        'out': tf.Variable(tf.random_normal([no_out]))
    }

# 5) Define Inference Model
with tf.name_scope("MLP_Model"):
    def multilayer_perceptron(x):
        with tf.name_scope("HL1"):
            layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
            layer_1 = tf.nn.relu(layer_1)
        with tf.name_scope("HL2"):
            layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
            layer_2 = tf.nn.relu(layer_2)
        with tf.name_scope("OutputLayer"):
            out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
        return out_layer
    # 5b) Construct Model
    y_pred = multilayer_perceptron(X)

# 6) Define Loss function (operation definition), incl. L2 Regularization
with tf.name_scope("Cost_regularized"):
    loss_op = tf.reduce_mean(tf.square(Y - y_pred)) + alpha*(\
              tf.nn.l2_loss(weights['h1'])+ \
              tf.nn.l2_loss(weights['h2'])+ \
              tf.nn.l2_loss(weights['out']))
    tf.summary.scalar("Test",loss_op)

# 6a) R^2 score (operation definition)
with tf.name_scope("R2_Score"):
    total_error = tf.reduce_sum(tf.square(Y - tf.reduce_mean(Y)))
    unexplained_error = tf.reduce_sum(tf.square(Y - y_pred))
    R_squared = tf.subtract(1.0, tf.div(unexplained_error,total_error))
    tf.summary.scalar("R2",R_squared)

# 7) Define Optimizer
with tf.name_scope("Training"):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate,name="Training")
    train_op = optimizer.minimize(loss_op)

# 8) Define Variable initializer
init_op = tf.global_variables_initializer()

# 9) Define Saver Operation for Ensamble Methods
with tf.name_scope("Ensemble"):
    saver=tf.train.Saver(max_to_keep=100)



####################################################################
##################### GRAPH Execution ##############################
####################################################################



with tf.Session() as sess:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter("logs/NN", sess.graph)
    for hy_par in [0.001]:
        for i in range (num_networks):
            print("Training Network %d"  %i)
            [X_train, X_val, y_train, y_val] = fct.splt_data(X_norm, y_data, 0.1)
            optimize(training_epochs, display_step, X_train, y_train,
                     X_val, y_val, reg_par=hy_par)
            saver.save(sess=sess, save_path=get_save_path(i))
        # Output final coefficients
        predictions = ensemble_predictions(num_networks,X_test)
        #mean_pred = predictions.reshape([num_networks,len(X_test)]).mean(0).reshape([len(X_test),no_out])
        #rqu = sess.run(R_squared,feed_dict={Y: y_test, y_pred: predictions})
        #print("#####Accuracy of ensemble network: %.2f #########" % rqu)
        # Plots
        #fct.predplot(y_hat=predictions, y_test=y_test)
        fct.predplot_time(y_real=y_test, y_hat=predictions[0,:], pltrange=144)
        ##
        predictions = ensemble_predictions(num_networks, X_norm)
        #mean_pred = predictions.reshape([num_networks, len(X_norm)]).mean(0).reshape([len(X_norm), no_out])
        #rqu_train = sess.run(R_squared, feed_dict={Y: y_data, y_pred: predictions})
        #Store results
        ## HERE STORE RESULTS
        f.write('##### For alpha = %.2d ##### \n' %hy_par)
        #f.write('Prediction Accuracy on Test Set is: %.2f \n' %rqu)
        f.write('##### For alpha = %d ##### \n' % hy_par)
        #f.write('Prediction Accuracy on Training Set is: %.2f \n' % rqu_train)
    writer.close()

f.write('-----------------------------------------------------------------\n \n')

f.close()
