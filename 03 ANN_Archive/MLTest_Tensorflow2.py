from __future__ import print_function


import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt
import functions as fct
import numpy as np



### Import Data
df_weather = pd.read_csv('test_steam.csv')
df_weather = df_weather.ix[:,  (df_weather.columns != 'Unnamed: 0')]

### collect previous cooling demand data
days=24
for i in range(1, days):
    mystr = '_lagged%d' % i
    df_weather = df_weather.join(df_weather['SumCh3Ch4'].shift(i), rsuffix=mystr)
del i

df_weather=df_weather.ix[days:,:]

####################################################################
##################### Helper Functions #############################
####################################################################
def get_save_path(net_number):
    return "checkpoint/network"+ str(net_number)


def optimize(training_epochs, display_step, X_train, y_train, X_test, y_test, reg_par=None):
    sess.run(init_op)
    fig = plt.figure()
    for i in range (training_epochs):

        if reg_par==None:       # Check if hyperparameter optimisation happening
            sess.run([train_op, loss_op], feed_dict={X: X_train, Y: y_train})
        else:
            sess.run([train_op], feed_dict={X: X_train, Y: y_train, alpha: reg_par})

        if i % display_step == 0:
            #print(i)
            pred = sess.run(R_squared, feed_dict={X: X_test, Y: y_test})
            plt.plot(i, pred, 'bx')
            pred = sess.run(R_squared, feed_dict={X: X_train, Y: y_train})
            plt.plot(i, pred, 'rx')

            # create summary
            result = sess.run(merged, feed_dict={X: X_train, Y: y_train, alpha: reg_par})
            writer.add_summary(result, i)
            plt.pause(0.1)
    print("Accuracy of Network")
    print(sess.run(R_squared, feed_dict={X: X_test, Y: y_test}))
    plt.close()
    return


def ensemble_predictions(num_networks,X_test):
    # Empty list of predicted labels for each of the neural networks.
    predictions = []

    # Classification accuracy on the test-set for each network.
    #test_score = []

    # For each neural network in the ensemble.
    for i in range(num_networks):
        # Reload the variables into the TensorFlow graph.
        saver.restore(sess=sess, save_path=get_save_path(i))

        # Print Networks Accuracy

        #Predictions
        pred = sess.run(y_pred, feed_dict={X: X_test})

        # Append the predicted labels to the list.
        predictions.append(pred)
    #predictions.reshape([num_networks, len(X_test)])
    return np.array(predictions)

####################################################################
##################### GRAPH ASSEMBLY ###############################
####################################################################

# 0) Network Parameters
### Static Parameters
learning_rate = 0.01
training_epochs =2000
display_step = 150

n_hidden_1 = 30
n_hidden_2 = 30

num_networks=50  #For ensamble predictions

### Parameters for Hyperparameter-Optimisation

alpha = tf.placeholder(tf.float32, None, name="Alpha")
#alpha=250

# 1) Derive Feature Vector and Output Vector
df_weather_x = df_weather.iloc[:,np.r_[0:24,72:99]]
ind = df_weather_x.ix[:, 'peak_dem']>0    # exclude zero cooling demand samples
X_data = df_weather_x.ix[ind,  (df_weather_x.columns != 'peak_dem')&
                           (df_weather_x.columns != 'CDD')].as_matrix()
y_data = np.reshape(df_weather_x.ix[ind, 'peak_dem'].as_matrix(),[554,1])
no_feat = len(X_data[1,:]) # no. of features
print(no_feat)

# 2) Normalize data, Training-Test split
X_norm, mean, std = fct.meanNormalization(X_data)
[X_norm, X_test, y_data, y_test] = fct.splt_data(X_norm, y_data, 0.1)



# 3) Define Placeholder (class definition)
X = tf.placeholder(tf.float32, [None, no_feat], name="X")
Y = tf.placeholder(tf.float32, [None,1], name="y")

# 4) Define Weights, Biases (class definition)
with tf.name_scope("Variable_Definition"):
    weights = {
        'h1': tf.Variable(tf.random_normal([no_feat,n_hidden_1]),name="HiddenLayer1"),
        'h2': tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2]),name="HiddenLayer2"),
        'out': tf.Variable(tf.random_normal([n_hidden_2, 1]),name="OutputLayer1")
    }

    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]),name="Bias"),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]),name="Bias"),
        'out': tf.Variable(tf.random_normal([1]))
    }

# 5) Define Inference Model
with tf.name_scope("MLP_Model"):
    def multilayer_perceptron(x):
        with tf.name_scope("HL1"):
            layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
            layer_1 = tf.nn.relu(layer_1)
        with tf.name_scope("HL2"):
            layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
            layer_2 = tf.nn.relu(layer_2)
        with tf.name_scope("OutputLayer"):
            out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
        return out_layer
    # 5b) Construct Model
    y_pred = multilayer_perceptron(X)

# 6) Define Loss function (operation definition), incl. L2 Regularization
with tf.name_scope("Cost_regularized"):
    loss_op = tf.reduce_mean(tf.square(Y - y_pred)) + alpha*(\
              tf.nn.l2_loss(weights['h1'])+ \
              tf.nn.l2_loss(weights['h2'])+ \
              tf.nn.l2_loss(weights['out']))
    tf.summary.scalar("Test",loss_op)

# 6a) R^2 score (operation definition)
with tf.name_scope("R2_Score"):
    total_error = tf.reduce_sum(tf.square(Y - tf.reduce_mean(Y)))
    unexplained_error = tf.reduce_sum(tf.square(Y - y_pred))
    R_squared = tf.subtract(1.0, tf.div(unexplained_error,total_error))
    tf.summary.scalar("R2",R_squared)

# 7) Define Optimizer
with tf.name_scope("Training"):
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate,name="Training")
    train_op = optimizer.minimize(loss_op)

# 8) Define Variable initializer
init_op = tf.global_variables_initializer()

# 9) Define Saver Operation for Ensamble Methods
with tf.name_scope("Ensemble"):
    saver=tf.train.Saver(max_to_keep=100)



####################################################################
##################### GRAPH Execution ##############################
####################################################################



with tf.Session() as sess:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter("logs/NN", sess.graph)
    for hy_par in [0.01, 0.1, 1.0, 10.0, 50.0, 100.0, 150.0, 200.0, 500.0, 10000.0]:
        for i in range (num_networks):
            print("Training Network %d"  %i)
            [X_train, X_val, y_train, y_val] = fct.splt_data(X_norm, y_data, 0.1)
            optimize(training_epochs, display_step, X_train, y_train,
                     X_val, y_val, reg_par=hy_par)
            saver.save(sess=sess, save_path=get_save_path(i))
        # Output final coefficients
        predictions = ensemble_predictions(num_networks,X_test)
        mean_pred = predictions.reshape([num_networks,len(X_test)]).mean(0).reshape([len(X_test),1])
        rqu = sess.run(R_squared,feed_dict={Y: y_test, y_pred: mean_pred})
        print("#####Accuracy of ensemble network: %.2f #########" %rqu)
        #Plots
        fct.predplot(y_hat=mean_pred, y_test=y_test)
    writer.close()

