# This file uses existing data to predict cooling demand


import pandas as pd
import matplotlib.pyplot as plt
import time
import functions as fct
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
from matplotlib import rc
rc('text', usetex=True)
import numpy as np


##################################################################
        # Load and Preprocess Cooling Demand Data #
##################################################################

##################################################################
        # Load and Preprocess Existing Data #
##################################################################
# load existing weather data
df_weather_hourly = pd.read_excel('WeatherData/output_incl_coverage.xlsx',0)
df_rad_day = pd.read_csv('WeatherData/Radiation/SolarRadiation_daily_satellite_nasa')
# reduce hourly to daily samples, assumptions: predictions at midnight (or 7 o'clock in the mornings?)
max_ch3 = max(df_weather_hourly['Ch3Power'])
max_ch4 = max(df_weather_hourly['Ch4Power'])
print(max_ch3)
print(max_ch4)
no_days = 700
no_var = 6
df_weather = pd.DataFrame(index=range(no_days),columns=range(no_var*24))

for day in range(no_days):
    df_weather.ix[day,'peak_dem'] = max(df_weather_hourly.ix[24 * day:24 * (day + 1), 'SumCh3Ch4'])
    for hour in range(24):
        df_weather.ix[day,hour] = df_weather_hourly.ix[hour+24*day,'temp']
        df_weather.ix[day,hour+24] = df_weather_hourly.ix[hour+24*day,'humid']
        df_weather.ix[day,hour+48] = df_weather_hourly.ix[hour+24*day,'wind']
        df_weather.ix[day, hour + 72] = df_weather_hourly.ix[hour + 24 * day, 'clouds']
        df_weather.ix[day, hour + 96] = df_weather_hourly.ix[hour + 24 * day, 'fog']
        df_weather.ix[day, hour + 120] = df_weather_hourly.ix[hour + 24 * day, 'rain']
# for test set
# Sort by CDD (cooling degree days)
df_cdd = pd.DataFrame(data=0, index=range(len(df_weather)), columns=['CDD'])

for i in range(len(df_weather)):
    for ii in range(24):
        if (df_weather.iloc[i, ii] - 15) > 0:
            df_cdd.loc[i, 'CDD'] = df_cdd.loc[i, 'CDD'] + \
                                   (df_weather.iloc[i, ii] - 15) * 1 / 24

df_weather['CDD'] = df_cdd
df_weather['Rad'] = df_rad_day.values.astype('float')[0:no_days]
df_weather = df_weather[~pd.isnull(df_weather['Rad'])]

df_weather.to_csv('test_coverage.csv')

df_weather = pd.read_csv('test.csv')
df_weather = df_weather.ix[:,  (df_weather.columns != 'Unnamed: 0')]
##################################################################
        # Apply ML Process #
##################################################################
# 1) Derive Feature Vector and Output Vector
#X,y = fct.deriveFeatures(df_weather.loc[:, 'temp':'wind'], df_weather.loc[:, 'SumCh3Ch4'], 24, 24)
#X['CDD'] = X['temp'] - 18
index = (df_weather.CDD >= 0)
X = df_weather.ix[index,  (df_weather.columns != 'peak_dem')]
y = df_weather.ix[index, 'peak_dem']

# 2) Preprocessing

# 3) Normalize Data
# here: apply mean normalization
X_norm, mean, std = fct.meanNormalization(X)

# 4) Split into Test and Training Set
for no_sam in np.arange(0.1,1.0,.1):
    [X_train, X_test, y_train, y_test] = fct.splt_data(X_norm,y,no_sam)

    # 5) Learning (including cross validation)
    # Neural Network
    t = time.time()
    no_layer=1
    no_units=200
    max_iter=10000
    alpha=5000
    #tuned_parameters = {'alpha':[10,100,1000,5000,10000]}
                        #'hidden_layer_sizes':[(100),(100,100),(10),(10,10)]}
    #reg=GridSearchCV(MLPRegressor(hidden_layer_sizes=(100,100),solver='adam', max_iter=max_iter, verbose=True, early_stopping=False),

                     #tuned_parameters,iid='False',cv=5,n_jobs=4)

    reg = MLPRegressor(solver='adam', hidden_layer_sizes=(30,30),
                       alpha=alpha, max_iter=max_iter, verbose=False, early_stopping=False)
    reg.fit(X_train,y_train)

    #print(reg.best_params_)
    #print(reg.cv_results_)

    elapsed = time.time() - t
    print(elapsed)


    # 6) Make Predictions
    y_hat=reg.predict(X_test)
    y_hat_train=reg.predict(X_train)


    # 7) Compute Performance
    # R^2 score
    score_test = reg.score(X_test,y_test)
    str = 'Test R^2 score:%f' %score_test
    print(str)


    score_test = reg.score(X_train,y_train)
    str = 'Train R^2 score:%f' %score_test
    print(str)

    # error + std

    error_test = np.mean(y_test.values - y_hat)
    std_test = np.std(y_test.values - y_hat)
    #print(error_test)
    #print(std_test)



##################################################################
        # Plot Data #
##################################################################
# denormalise input data
X_test = fct.reversedmeanNormalization(X_test, mean, std)
X_train = fct.reversedmeanNormalization(X_train, mean, std)

plt.figure()
plt.plot(X_test['CDD'].values,y_test.values,'rx',label='real')
plt.plot(X_test['CDD'].values,y_hat,'x',label='predicted')
plt.xlabel('Cooling Degree Days')
plt.ylabel(r'Cooling Demand [t]')
plt.legend()
plt.show()


plt.figure()
plt.plot(X_train['CDD'].values,y_train.values,'rx',label='real')
plt.plot(X_train['CDD'].values,y_hat_train,'x',label='predicted')
plt.xlabel('Cooling Degree Days')
plt.ylabel(r'Cooling Demand [t]')
plt.legend()
plt.show()



#
# plt.figure()
# df_demand.loc[:,'Cooling Demand Chiller 1'].plot()
# df_demand.loc[:,'Cooling Demand Chiller 2'].plot()
# plt.xlabel('Hours')
# plt.ylabel(r'$CH_3/CH_4$ Demand [t]')
# plt.show()